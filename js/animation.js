/*
	Bei Animationen wird eine Funktion z. B 60 x in der Sekunde aktualisiert.

	setTimeout ist aus zwei Gründen dafür nicht gut geeignet:
	- es ist nicht präzise genug für bestimmte Berechnungen
	- die Framerate Zeit ist nicht in sync mit der Monitor framerate - kann zu Rucklern führen
*/
// const frameRate = 1 / 60;
// console.log(timerId);
const ball = document.querySelector(".ball");
const maxDistance = 500;
const speed = 2;

function animateBall() {
	const rect = ball.getBoundingClientRect();
	ball.style.left = rect.x + speed + "px";
	if (rect.x <= maxDistance) {
		// Rekursive Funktion: eine sich selbst aufrufende Funktion.
		// requestAnimationFrame kümmert sich darum, dass die Funktion in sync mit
		// der Monitor refresh rate aufgerufen wir.
		window.requestAnimationFrame(animateBall);
	}
}

window.requestAnimationFrame(animateBall);
