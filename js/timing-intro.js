const start = Date.now();
console.log(start);

// Führt eine Funktion 1x nach einer gewissen Zeit (in ms) aus.
setTimeout(() => {
	const delta = (Date.now() - start) / 1000;
	console.log(`Vergangene Sekunden: ${delta}`);
}, 2000);

console.log("noch nicht das Ende");

// setInterval funktioniert ähnlich wie setTimeout. Sie führt eine callback Funktion in bestimmten
// Intervallen (in ms definiert) aus.
const max = 5;
let counter = 0;

const timerId = setInterval(() => {
	const delta = (Date.now() - start) / 1000;
	console.log(`Vergangene Sekunden: ${delta}`);
	counter++;
	if (counter >= max) {
		// Um einen Prozess setInterval zu stoppen, verwenden wir clearInterval(id)
		clearInterval(timerId);
	}
}, 2000);

